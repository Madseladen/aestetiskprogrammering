let vol;
let ctracker;
let capture;
let input;
let lineWidth = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);

  capture=createCapture(VIDEO);
  capture.size(1600,800);
  capture.hide();

  ctracker=new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  input = createInput();
  input.position(700,20);
}

function draw() {
  image(capture,0,0,1600,800);

  vol = mic.getLevel();
  let y = map(vol, 0, 1, height, 0);

  strokeWeight(lineWidth);
  stroke(255, 0, 0); // red color
  line(width - lineWidth/2, height, width - lineWidth/2, y);

  textSize(32);
  text(input.value(),0, windowHeight/2);

  if(mouseIsPressed===true){
    filter(INVERT);
  }
  
  let positioner=ctracker.getCurrentPosition();
  if(positioner && positioner.length){
    for(i=0;i<positioner.length;i++){
      ellipse(positioner[i][0],positioner[i][1],5)
    }
  }
}

}
