# minix4 af Mads Leth Hansen

## Billede + links
Før man har gjort noget:
![](minix41.png)
Efter:
![](minix42.png)

[Link til at køre programmet](https://madseladen.gitlab.io/aestetiskprogrammering/minix4/index.html)


[Link til koden bag det](https://gitlab.com/Madseladen/aestetiskprogrammering/-/blob/main/minix4/index.html)




## Titel og kort beskrivelse af mit værk.

Min titel på mit minix4 må være noget allá "I see you".
Vi højere og højere grad i den digitale verden, om det er på arbejdet, i skolen eller i fritiden, så har vi næsten altid en forbindelse til den digitale verden. Og selvom denne digitale verden er en velkommende og meget brugbar ressource, så er det ikke ofte, vi tænker over, hvad vi egentlig "betaler" for denne service- For det gør vi. Jeg tænkte personligt ikke selv over, hvor meget af mit liv en computer har adgang til bare ved et par enkelte tryk. Denne aflevering er blevet lavet på baggrund af disse tanker; Tanker omkring, hvor meget data computere har af dit liv, og hvor meget du ikke ved, de har adgang til. Med programmet ses det hurtigt, det er brugeren, der er baggrunden på siden, og når brugeren taster ord ind i søgefeltet ses det også, at ordene med det samme ryger op på skærmen igen. Imens kommer der en masse forskellige udsagn op på skærmen, som skal simulere computerens konstante forsøg på at dømme brugeren og sætte personen i en retmæssig kasse.

Mit værk er ikke ment som provokerende, men måske mere oplysende omkring, hvor meget en computer reelt har på dig, og hvad du siger ja til uden at tænke over det, hver eneste dag. Det viser nemlig en del af de måder, som en comuter kan indsamle data omkring dig på.

## Mit program og hvad jeg har lært.
JEg har lavet et program, der først og fremmest bruger computeren webcam til at filme personen bag skærmen. Denne person bliver dertil også tracket på en hel række parametre i deres ansigt. Dertil har jeg lavet en `if-statement` til både `keyIsPressed` og `mouseIsPressed`. Når brugeren trykker med musen bliver alle farverne omvendte. Når brugeren bruger tastaturet sker der 2 ting: Det de skriver kommer til at stå på tværs af skærmen samt en masse forskellige phraser bliver slynget rundt på skærmen.

Programmet der tracker personen, der sidder bag ved skærmens ansigt. Denne funktion ser således ud:
```
 capture=createCapture(VIDEO);
  capture.size(1600,800);
  capture.hide();

  ctracker=new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
```
Denne række af funktioner samt `image(capture,0,0,1600,800);`gjorde det muligt for mig at indsætte den live video af personen bag skærmen. JEg brugte også en syntaks til netop at finde de vitale punkter i et menneskets ansigt:
```
      let positioner=ctracker.getCurrentPosition();
if(positioner.length){
  for(i=0;i<positioner.length;i++){
      ellipse(positioner[i][0],positioner[i][1],5,)
  }
```
Med alle disse linjers kode var det muligt for mig at lave det canvas med brugeren som baggrund.
Jeg brugte dertil også et filter samt forskellige omgange af tekst til at få brugeren til også at bruge keyboardet som input, og i og med der er en boks, brugeren skal trykke i kommer brugeren også til at bruge musen. Dermed får de alle syntakse at opleve.

## Mine tanker omkring data og dataindsamling

Jeg tror jeg er meget todelt omkring hele temaet med dataindsamling. Jeg synes det er skræmmende at høre/se, hvor meget data computeren egentlig kan få af dig på meget hurtig vis, hvoraf jeg også selv har oplevet det, den helt klassiske med reklamer. Dog synes jeg også det er smart, at computeren gør det; det er effektivisering af det at gøre AI smartere, det er prisen for at bruge disse tjenester. Det vil tage folkene bag AI mange mange års arbejde at indsamle og uddanne deres robotter, men i og med vi alle sammen uddanner dem igennem internettet gør, at vi accelererer denne process. Jeg kan sagtens se, hvorfor nogen synes det er forrykt, at firmaerne ikke er særlig upfront med, hvad de bruger deres data til, men jeg tror så længe jeg kan leve i troen om, de ikke bruger min data indsindet, og det ikke bliver sporet tilbage til mig, så har jeg det fint med at betale min data som tak for at bruge deres services.
