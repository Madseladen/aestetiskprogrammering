# miniX2 af Mads Leth Hansen

## Hvad har jeg lavet?

Jeg har lavet en kode, der ved første øjekast viser den samme emoji 3 gange i streg med teksten "Er vi mest repræsentative?". Hvis der trykkes og holdes inde på musen kan brugeren derefter se, at der kommer 3 nye emojis frem, som er af 3 forskellige farver, hovedstørrelser og forme. OVer de 3 står der "Eller er vi?".

## Link og billede af min miniX2

[Link til mit miniX2](https://madseladen.gitlab.io/aestetiskprogrammering/miniX2/index.html)

[Link til source koden](https://gitlab.com/Madseladen/aestetiskprogrammering/-/blob/main/miniX2/minix2.js)

Her ses det første møde med min miniX2
![](miniX21.png)

Her er, hvad der ses, hvis der trykkes på musen.
![](miniX22.png)

## Hvordan har jeg gjort det?

Efter jeg havde lavet baggrunden lavede jeg den første midterste emoji.
Jeg startede med at tilsætte den karakteristiske gule farve til emojien og dernæst lavede formen.

```
 stroke(5);
fill(255,255,55);
ellipseMode(CENTER);
ellipse(700,400,400);
```
Jeg brugte `ellipseMode(CENTER)` til at centrere ellipsen omkring mit punkt, som er 700,400.

Dernæst var det tid til at lave øjnene. Punkterne til øjnene fandt jeg ved først at gå 100 pixels ud til hver side på x-aksen. Dernæst tænkte jeg, at øjnene skulle op i ansigtet, hvilket er derfor jeg kom frem til 350.

```
stroke(3);
fill(255);
ellipse(600,350,100);
ellipse(800,350,100);
```
Emojien skulle også have popiller og en mund, hvilket er, hvad den nedstående kode gjorde. Begge dele skulle være sort, så derfor står der `fill(0,0,0)` ovenfor. Jeg kunne sagtens bare have skrevet `fill(0)`, men opdagede det for sent til at jeg gad at rette det.

```
fill(0,0,0)
ellipse(600,355,50);
ellipse(800,355,50);
arc(700,440,150,100,0,PI);
```
Til sidst lavede jeg el lille hvid prik inde i de to popiller, hvilket er hvad, der står beskrevet i nedstående kode.
```
stroke(2);
fill(255);
ellipse(612,350,18);
ellipse(812,350,18);
```
Dette gjorde jeg 3 gange med forskellige koordinater til at lave 3 identiske emojies. Jeg tegnede det bevidst i `function draw`istedet for `function setup()`, da det hele tiden skal komme tilbage til de emojis, efter en bruger har interageret med det.

For at lave de andre 3 emojis lavede jeg en if statement. Her brugte jeg ` if(mouseIsPressed === true)`, hvilket betyder, at hvis min mus er trykket og indtil den ikke er trykket på længere, skal den vise noget andet.

Jeg kodede derefter de 3 anderledes emojis på samme vis som den første, dog bare under denne ` if(mouseIsPressed === true)`.

Til disse emojis tilføjede jeg også nogen øjenbryn, som skulle gøre, de så sure ud.
```
 strokeWeight(20);
    line(1015,280,1110,310);
    line(1290,280,1185,310);
```
`StrokeWeight`bestemmer, hvor tyk linjen skal se ud, og de to andre er x- og y-koordinater for linjens start og slutpunkt.

Det sidste stykke kode jeg ikke har forklaret er tekstkoden. Jeg har både en udenfor min ` if(mouseIsPressed === true)` og udenfor den. De opstiller to forskellige udsagn, som brugeren kan tage stilling til.
```
strokeWeight(1);
  textSize(50);
  textFont('Times New Roman');
  text('Eller er vi?',575,50);
```
`textSize`bestemmer tekstens størrelse samt `textFont`der bestemmer, hvilken skrifttype, der skal stå på lærredet. Under `text` skrev jeg, hvad der skulle stå, samt x-og y-koordinater for, hvor teksten skulle starte.

## Hvor vigtig synes jeg, det er, at emojis kan repræsentere alle?

I takt med flere og flere emojis bliver tilføjet til både Apple's, Google's, Meta's osv. skriftsprog, så rejser spørgsmål om enklusion, sexisme, generalisering osv., også. Stereotyper af, hvordan man skal se ud, hvis man er glad, hvis man danser osv., kan gøre, at det kan være meget svært at føle sig inkluderet og repræsenteret i verdenen af emojis.

Personligt har jeg en meget meget begrænset brug af emojis og bruger dem så lidt som muligt, så jeg har muligvis også en meget farvet mening omkring hele denne her debat omkring generalisering og stereotyper.

Jeg tror på, desto flere emojis der bliver tilføjet desto mindre føler folk sig repræsenteret. Dengang der kun var få emojis som glad, sur, ked, tommel op, hjerte og får andre var det de valg, befolkningen havde, og som de måtte affinde sig med at have. I takt med der nærmest er kommet en emoji til alt gør det meget mere ekskluderende ikke at have en emoji for ens specifikke erhverv, følelse, hobby eller hvad det nu kunne være. Denne, i princippet, gode intention udviklerne havde omkring at inkludere flere med emojis kan have den modsatte effekt over for befolkningen, hvis de ikke føler sig repræsenteret.

Mit miniX projekt er lavet meget på baggrund af min holdning omkring emojis. Jeg synes de emojis vi har nu er fint repræsentative, og jeg føler bestemt de kunne være **meget** mere generaliserende og værre. Jeg startede med at prøve at lave en emoji, der repræsenterede alle, men fandt hurtigt ud af, at det var super svært, så derfor gik jeg den helt modsatte vej. Jeg gik derfor efter nogen meget mere generaliserende emojis, som groft skulle forestille forskellige etniciteter. Dette var for forhåbentligt måske at provokere lidt og vise, at emojis kan være meget mere generaliserende og ikke i orden.

