
function draw(){
//Her laves kanvasset, jeg laver det i draw, fordi den hele tiden skal tegne den, hvis ikke min mus bliver holdt inde.
  createCanvas(windowWidth,windowHeight);
background(255);

//Midter smiley
stroke(5);
fill(255,255,55);
ellipseMode(CENTER);
ellipse(700,400,400);
stroke(3);
fill(255);
ellipse(600,350,100);
ellipse(800,350,100);
fill(0,0,0)
ellipse(600,355,50);
ellipse(800,355,50);
arc(700,440,150,100,0,PI);
stroke(2);
fill(255);
  ellipse(612,350,18);
  ellipse(812,350,18);

//højre smiley
stroke(5);
fill(255,255,55);
ellipseMode(CENTER);
ellipse(1150,400,400);
stroke(3);
fill(255);
ellipse(1050,350,100);
ellipse(1250,350,100);
fill(0,0,0)
ellipse(1050,355,50);
ellipse(1250,355,50);
arc(1150,440,150,100,0,PI);
stroke(2);
fill(255);
  ellipse(1262,350,18);
  ellipse(1062,350,18);
  
//Venstre smiley
stroke(5);
fill(255,255,55);
ellipseMode(CENTER);
ellipse(250,400,400);
stroke(3);
fill(255);
ellipse(150,350,100);
ellipse(350,350,100);
fill(0,0,0)
ellipse(150,355,50);
ellipse(350,355,50);
arc(250,440,150,100,0,PI);
stroke(2);
fill(255);
  ellipse(162,350,18);
  ellipse(362,350,18);

  fill(0);
  strokeWeight(1);
  textSize(50);
  textFont('Times New Roman');
  text('Er vi mest repræsentative?',450,50);

if(mouseIsPressed === true){
  //Generaliserende mørk emoji
  background(255);
  stroke(5);
  fill(70,49,19);
  ellipseMode(CENTER)
  ellipse(700,400,350,550)
  stroke(3);
  fill(255);
  ellipse(600,350,100);
  ellipse(800,350,100);
  fill(0,0,0)
  ellipse(600,330,50);
  ellipse(800,330,50);
  arc(700,490,150,100,PI,0);
  fill(255);
  ellipse(612,330,18);
  ellipse(812,330,18);
  strokeWeight(20);
stroke(0);
line(565,280,660,310);
line(840,280,735,310);


  //generaliserende gul emoji
  strokeWeight(1);
  stroke(5);
  fill(255, 255, 143);
  ellipseMode(CENTER);
  ellipse(250,400,450,350);
  stroke(3);
  fill(255);
  ellipse(150,350,100,70);
  ellipse(350,350,100,70);
  fill(0);
  ellipse(158,343,50);
  ellipse(358,343,50);
  arc(250,490,150,100,PI,0)
  fill(255);
  ellipse(172,350,18);
  ellipse(372,350,18);
  strokeWeight(20);
  stroke(0);
  line(115,290,210,320);
  line(390,290,285,320);
  
  //Generaliserende hvid emoji
  strokeWeight(1);
  noStroke()
  fill(255,224,189);
  rectMode(CENTER)
  rect(1150,300,400,225)
  ellipseMode(CENTER);
  ellipse(1150,400,400);
  stroke(3);
  fill(255);
  ellipse(1050,350,100);
  ellipse(1250,350,100);
  fill(0,0,0);
  ellipse(1040,330,50);
  ellipse(1240,330,50);
  arc(1150,490,150,100,PI,0);
  fill(255);
  ellipse(1252,340,18);
  ellipse(1052,340,18);

  strokeWeight(20);
  line(1015,280,1110,310);
  line(1290,280,1185,310);
  
  fill(0);
  strokeWeight(1);
textSize(50);
textFont('Times New Roman');
text('Eller er vi?',575,50);

}
}


