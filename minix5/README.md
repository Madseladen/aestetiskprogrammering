# minix5 af Mads Leth Hansen

[RunMe](https://madseladen.gitlab.io/aestetiskprogrammering/minix5/index.html)

![](minix5.png)

## Hvilke regler er der i mit program?

[Bedre overblik over koden her](https://gitlab.com/Madseladen/aestetiskprogrammering/-/blob/main/minix5/index.html)

Jeg startede ud med at gå i kæmpe tænkeboks over, hvordan jeg skulle gribe opgaven an. Jeg havde en forskellige idéer oppe at vende, men 1 ting gik igen ved alle mine idéer, hvilket var, det helst skulle være så tilfældigt som muligt. Det ledte mig ind i dette projekt. Jeg har lavet en kode, der fylder skærmen relativt hurtigt med forskellige geometriske forme, og bliver ved med at fylde ovenpå. Jeg synes det giver en meget livlig oplevelse, når man åbner programmet, hvilket jeg også gik efter. Næsten alt ved programmet er tilfældigt inden for de regler, som jeg har opstillet.


Programme bruger flere regler. Den bruger et `forLoop` til at tegne 50 forskellige former i hver frame. Inde i dette `forLoop` bruges der også andre regler såsom `let formType = int(random(4))` Denne regel gør, at programmet skal tage et tilfældigt tal imellem 0-3, og med `int()` funktionen rundes der op til hele tal fremfor decimal tal.


Disse 4 forskellige udfald afgører, hvilken en form, der skal blive tegnet på det givne punkt. Et eksempel på dette er følgende:
```
if (formType == 0) {
      // Laver firkanten
      fill(random(255), random(255), random(255));
      rect(x, y, random(100), random(100));

```
Hvis programmet ender på tallet 0, så skal den lave en firkant med en tilfældig farve i RGB, et tilfældigt sted på canvasset med en tilfældig størrelse imellem 0 og 100.

Dette gør programmet 50 gange per frame, og derved farves canvasset. Det er de regler som gør, at programmet kører således, som det gør.

## Refklektioner omkring regler og generativ kode

Monfort et al. beskriver randomness således: "In the most abstract sense, we can think of randomness as a lack of pattern, a way of generating information that is not predictable or generated according to a specific rule." (Montfort et al 2012, s. 119). Personligt synes jeg altid der e noget tiltalende ved en `random`funktion. Det er både fordi den gør noget helt tilfældigt og uforudsigeligt som nævnt i citatet, men også fordi jeg ikke skal tage et valg, det gør computeren for mig.

Som vi fik introduceret i timen og i teksten, så er random funktionen, som jeg har brugt ikke reelt random i og med den er lavet ud fra en matematisk funktion. ""A pseudorandom number generator (PRNG) is a deterministic algorithm that generates a sequence of numbers that appear to be random, but are actually produced by a set of mathematical rules." (Montfort et al 2012, s. 121). Dette synes jeg er skørt/sjovt at tænke på især, når jeg tænkte, den var helt random. Dette kan også drages videre over til det vi før har talt om omkring en computers subjektivitet.

I mit arbejde med denne random funktion har denne pseudo randomness fungeret rigtig godt, og det har skabt denne uforudsigelige opførsel. Programmet er heller ikke det samme nogen gange, det ændrer sig altid per gange, man åbner det, hvilket gør hvert program unikt. Dette synes jeg er det spændende med generativ kode, at det ikke giver det samme udtryk hver gang men skifter hver gang. Jeg forsøgte mig med noise funktionen istedet for random, da den kører andereldes end random, men gav ikke det samme "vilde" udtryk, som min random funktion gjorde.

