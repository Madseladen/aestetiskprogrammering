function setup() {
  createCanvas(windowWidth, windowHeight);
  noStroke();
  background(0);
  frameRate(10);
}

function draw() {
  // Den skal lave 50 af de valgte forme per frame
  for (let i = 0; i < 50; i++) {
    // Generere tilfældige x og y koordinater for til figurerne.
    let x = random(width);
    let y = random(height);
    // Tager tilfældige former nedenfor
    let formType = floor(random(4)); // Vælger et tilfædigt tal imellem 0 og 3 og runder hen til nærmeste hele tal.
    if (formType == 0) {
      // Laver firkanten
      fill(random(255), random(255), random(255));
      rect(x, y, random(100), random(100));
    } else if (formType == 1) {
      // Laver cirklen
      fill(random(255), random(255), random(255));
      ellipse(x, y, random(100), random(100));
    } else if (formType == 2) {
      // Laver trekanten
      fill(random(255), random(255), random(255));
      triangle(x, y, x + random(100), y + random(100), x + random(100), y - random(100));
    } else {
      // Tegner en linje
      stroke(random(255), random(255), random(255));
      strokeWeight(random(5));
      line(x, y, x + random(50), y + random(50));
    }
  }
  //Gør dette 50 gange per gang den kører rundt pga. forloopet. 
}
