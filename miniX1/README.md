# MiniX1 af Mad Leth Hansen

## Hvad har jeg lavet?

Jeg har lavet et 500x500 canvas med en sort baggrund, hvorpå hver andet felt fremtræder mere og mere lyst desto mere brugeren trykker på den højre piletastsknap. Brugeren kan også vælge at trykke på den venstre piletastsknap, som gør, at felterne bliver mørkere og mørkere.

Dette skal forestille et skakbræt, som bliver mere og mere synligt. Hvis brugeren trykker på højre piletastsknap for mange gange, så vil det ende ud i, at skakbrættet bliver helt mørkt igen, og brugeren igen skal trykke på højrepiletast for at kunne se skakbrættet endnu en gang.

### Billede og link til mit miniX1
Her er, hvordan canvasset ser ud efter 2 tryk.
![](Start_minix1.png)


Her er hvordan canvasset kan se ud efter en en del tryk.
![](End_MiniX_1.png)

***Husk højre - mere lyst, venstre - mindre lys***

 [https://madseladen.gitlab.io/aestetiskprogrammering/miniX1/index.html]
 

## Hvordan virker koden?

Som nævnt før startede jeg blot med at indsætte et canvas på 500x500 pixels, som skal være sort. Nedenfor er denne process vist.
```
function setup() {
  createCanvas(500,500);
  background(0,0,00);

```
Detnæst skulle jeg lave de hvide felter, som skulle være kvadratiske. Nedenfor er der beskrevet, hvordan jeg opsatte en linje af de hvide rubrikker på den sorte baggrund.

```
let value = 0;
function draw() {
  push();
  fill(value);
  rect(0, 0, 50, 50);
rect(100, 0, 50, 50);
rect(200, 0, 50, 50);
rect(300, 0, 50, 50);
rect(400, 0, 50, 50);
```
Som det også fremgår i koden startede jeg med at sætte valuen/væriden til 0. Dette gjorde jeg, da det var farvekoden til sort. Dernæst indsatte jeg også, at farven på valuen skulle fill alle de fremtidige rect. 

Jeg har også indsat en push værdi, da jeg havde tænkt, jeg skulle lave en anden funktion, men det blev aldrig aktuelt, og da push og pop funktionen ikke påvirker min kode valgte jeg blot at beholde dem.

Nu vil jeg forklare, hvordan koden får de skiftevise rubrikker til at blive lysere og lysere:
```
function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    value = value-5;
  } else if (keyCode === RIGHT_ARROW) {
    value = value + 5;
    if(value>255){
      value=0;
    }
  }
  pop();
```

Øverst i dette kodeafsnit sætter jeg fast, at det er en funktion af, når en bestemt tastatursknap bliver trykket på. Dernæst opsatte jeg en if statement.

Inde i den if statement skrev jeg først, at hvide den venstre piletast bliver trykket ned, så skal farven i hver andet felt skifte farve til en farve på sort-hvid farvespektrummet, som er 5 mindre en den nuværende farve. Det vil altså sige at farven på felterne bliver mørkere, hvide den venstre piletast bliver trykket på.

Dernæst er der en else if statement, som siger, at hvide den højre piletast bliver trykket ned, så skal den gå 5 værider op i det sort-hvide farvespektrum. Dette betyder ligeså, at hver andet felt bliver lysere i takt med, at der trykkes på den højre piletast.

Til sidst står der endnu en if statement, der siger, at hvis valuen bliver mere end 255, hvilket er max-værdien i farvekoden, så skal den går tilbage til den oprindelige value, som er 0. Det vil altså sige, at når den højre knap er blevet trykket på 51 gange, så har den ppnået sin maksværdi, altså så hvid, som feltet kan blive, og hvis knappen bliver trykket på en gang mere, så bliver canvaset helt sort igen.

## Mine tanker om det at kode for første gang.

Jeg tror jeg vil sammenligne kodning med matematik. Kodning føler jeg er meget ligesom matematik; der er altid kun 1 rigtig svar, og midler man kommer han til svaret er igennem nogen formler i matematik og formulerer i kodning. Det vil også sige, at hvis man fejler et sted i regnestykket i matematik, så får du ikke det rigtige svar, og det samme gælder kodning. Kodning adskiller sig fra matematik på den måde, at koden fortæller, hvor det går galt henne. Med matematik får du altid et svar, men med kodning får du ikke et svar, hvis det er lavet forkert, så virker det simpelthen bare ikke.

Jeg utrolig meget hjælp på https://p5js.org/reference/, hvor jeg både så den kode, jeg skulle bruge for at få mit ønskede resultat, men også forklaringerne og de eksempeler, hvor de viste, hvordan man kunne bruge koden i relation til andre koder.

Jeg havde tænkt mig at lave et roterende skakbræt, men da jeg prøvede at programmere det, fungerede det slet ikke for mig. Jeg prøvede at indsætte forskellige cubes, men der kom ikke flere frem end kun en. Derfor måtte jeg nøjes med mit 2 diminisionelle skakbræt, som ses via linket og på billede ovenfor.

Min umildbare følelser omkring kodning er meget positivt. Jeg kan rigtig godt lide idéen om, at der kun er et svar, og du derfor altid kan gå tilbage i din kode og finde ud af, hvor det gik galt, og hvorfor det gik galt. Jeg kan også godt lide den meget "legende" tilgang man kan have med kodning. Med det mener jeg, at jeg har brugt meget trial and error til at programmer både dette projekt, men også bare at sidde og hygge med at programmere, hvadend der lige var sjovt og overkommeligt for mig.
