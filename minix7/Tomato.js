class Tomato{
    constructor(){
        this.posX=random(0,width);
        this.posY=random(0,height);
        this.speed=random(2,4);
        this.size=50;
    }

    show(){
        image(krog,this.posX, this.posY,this.size,this.size);
    }

move(){
    this.posY+=this.speed
    if(this.posY>height+50){
        this.posY=random(-100,-50);
        this.posX=random(0,450);
    }
}
}