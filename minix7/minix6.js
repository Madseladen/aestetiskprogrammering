let seddel = [];
let player;
let point = 0;
let liv = 3;
let antal = 10;
let bank;
let coin;
let haj;
let tomato;
let images;
let krog;
let startScreen = true; //Laver startsskærm

function drawStartScreen() {
  background(0,0,30);
  fill(255);
  textSize(32);
  textAlign(CENTER);
  text("Velkommen til hajspillet", width / 2, height / 2 - 50);
  textSize(16);
  text("Spis fiskene inden de rammer bunden",width/2,height/2)
  text("Undgå krogen!!",width/2,height/2+30);
  textSize(18);
  text("Tryk på en vilkårlig knap for at starte", width / 2, height / 2 + 100);
  image(haj,width/2-35,height/2+120,70,70);
  image(images,width/2+70,height/2-150,70,70)
  image(images,width/2+140,height/2-150,70,70)
  image(images,width/2-140,height/2-150,70,70)
  image(images,width/2-70,height/2-150,70,70)
  image(images,width/2,height/2-150,70,70)
  image(images,width/2-210,height/2-150,70,70)
  image(images,width/2+210,height/2-150,70,70)
  image(images,width/2-280,height/2-150,70,70)
  image(krog,width/2+100,height/2,70,70);

}

function keyPressed() {
  startScreen = false;
}


function preload(){
    haj=loadImage("haj.gif");
    images= loadImage('fisk5.gif');
    krog=loadImage('krog.gif')
   
  }

function setup() {
    createCanvas(500, 500);
    for (let i = 0; i < antal; i++) {
        seddel.push(new Seddel());
    }
    
    tomato=new Tomato();
    player = new Player();
 
  

}

function draw() {
    if (startScreen) {
        drawStartScreen();
      } else {
    background(0,0,30)
    spawnSeddel();
    spawnTomato();
    player.show();
    checkCollision();
    checkBunden();
    sammenstoed();
    textAlign(CENTER,CENTER);
    fill(255);
    textSize(20);

    text("Fisk spist:  "+point, 70, 50)
    text("Liv: "+liv,470,50);

    if(keyIsDown(UP_ARROW)){
        player.moveUp();
    } else if(keyIsDown(DOWN_ARROW)){
        player.moveDown();
    }
    if(keyIsDown(LEFT_ARROW)){
        player.moveLeft();
    }else if(keyIsDown(RIGHT_ARROW)){
        player.moveRight();
    }
}
}
function spawnSeddel() {
    for (let i = 0; i < seddel.length; i++) {
        seddel[i].move();
        seddel[i].show();
    }
}

function spawnTomato() {
        tomato.move();
        tomato.show();
    }


function checkCollision() {
    let distance;
    for (let i = 0; i < seddel.length; i++) {
        distance = dist(player.posX + player.size/10, player.posY + player.size/10, seddel[i].posX, seddel[i].posY);

        if (distance < 50-player.size/2) {
            seddel.splice(i, 1);
            point++
            seddel.push(new Seddel());
        }
      
    }
    
}
function sammenstoed() {
    let distance1;
    distance1 = dist(player.posX + player.size / 10, player.posY + player.size / 10, tomato.posX, tomato.posY);
    console.log(distance1)
    if (distance1 < player.size / 10 + tomato.size / 2) {
      point=0
    }
  }

function checkBunden(){
    for(let i=0; i<seddel.length;i++){
        if(seddel[i].posY>height){
            liv--;
        seddel.splice(i,1);
        console.log("Hej");
        if(liv<=0){
            noLoop();
            push();
            fill(255);
            textAlign(CENTER,CENTER);
            textSize(30);
            text("For mange fisk slap forbi",width/2,height/2);
            textSize(14);
            text("Tryk 'command'+'r' for at prøve igen",width/2,300)
            pop();
        }
        }
        
    }
    
    }