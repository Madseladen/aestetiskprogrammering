## MiniX7 af Mads Leth Hansen


[RunMe](https://madseladen.gitlab.io/aestetiskprogrammering/minix7/index.html)

Starten af programmet
![](minix7.png)


Slutningen af programmet
![](minix7slut.png)


[Bedre overblik over koden her](https://gitlab.com/Madseladen/aestetiskprogrammering/-/blob/main/minix7/minix6.js)


### Valg af Minix
Jeg valgte at genbesøge min nyligt afleverede minix6, da jeg følte den godt kunne tages et niveau op. Jeg havde også til overvejet at tage både min minix4 omkring Capture All, da jeg også synes det kunne være interessant at arbejde med den igen, men fandt hurtigt ud af, at jeg synes det kunne være mest spændende at lave om og optimere minix6.

### Ændringerne

Når jeg kigger på min minix6 ser jeg egentlig et velfungerende spil. Det har i princippet alt, hvad det skal have, hvad det er nødsaget til at have: et mål, en måde at styre og en måde at tabe på. Dog følte jeg den manglede mange af de mere nice-to-have features, som nu er blevet inkoorporeret i dette spil.

For det første har jeg lavet en startsskærm inden man starter spillet. Den lavede jeg med en custom funktion, som jeg kaldte´´function drawStartScreen()´´. Inde i den har jeg alt koden til startsskærmen, som byder spilleren velkommen, forklarer formålet med spillet og fortæller, hvordan man kommer igang med at spille. Jeg lavede en global værdi der hed ´´startScreen=true´´, og lavede en ´´function keyPressed() {
  startScreen = false; }´´, som gjorde at så snart brugeren trykker på en knap, så vil spillet blive startet.

En anden funktion der er tilføjet er en ny måde at tabe på eller ihverfald blive sat tilbage på. Det var endnu en costum funktion, som jeg kaldte ´´function sammenstoed´´, som tjekkede om spilleren og en nyligt tilføjede klasse kolliderede. Denne nye klasse skulle fungere som en slags fjende i spillet, som skal undgåes. Den nye klasse blev til en krog som skulle fange spilleren, der nu i stedet for at være en sparegris var en haj, som skulle fange fiskene i havet.

Jeg ændrede også på teksten, der står, når man starter samt baggrunden for at det passer bedre til dette undervandstema, jeg har lavet samt mængden af fisk der bliver spawnet i spillet.

### Hvad jeg har lært?

Mit hovedmål ved denne opgave var at lave min minix6 om til et mere komplet spil med flere features og virke som et mere færdigt spil. I denne opgave lærte jeg, hvordan jeg kan bruge flere classes end bare de 2, jeg havde i minix6. Jeg havde 3 classes og alle 3 gjorde noget forskelligt. Jeg fik også en bedre forståelse for, hvordan man kalder på de classes inde i sit spil samt hvordan man kan adskille disse classes egenskaber i forskellige custom funktioner inde i spil javascript filen. 

"[...] our intention is for readers to acquire key programming skills in order to read, write and think with, and through, code [...]" (Soon Winnie & Cox, Geoff, 2020). Denne måde at arbejde med programmering er noget jeg sandeligt har oplevet både ved denne opgave men også hele vejen igennem kursusets gang. Og denne tankegang er noget, jeg er blevet bedre og bedre til igennem kursusets gang, hvor jeg kigger og ændrer i kode for at finde ud af, hvor mine fejl er, hvilket netop også var tilfældet i opgaven, hvor jeg skulle adskille fisken og krogens egenskaber fra hinanden.

### Æstetisk programmering og digital kultur

Æstetisk programmering fokuserer på de politiske aspekter i programmering og digital kultur.

"We follow the principle that the growing importance of software requires a new kind of cultural thinking, and curriculum, that can account for, and with which to understand better, from within, the politics and aesthetics of algorithmic procedures, data processing, and abstracted modeling." (Soon Winnie & Cox, Geoff, 2020)

Med den voksende digitale kultur vi har i hele verden er vi nødsaget til at kunne forstå på et breddere niveau de konsekvenser de valg vi træffer, når vi designet og programmere. Intet er obkjektivt og alt sker ud fra individets syn, selvom de prøver at lave det objektivt er det stadig individets subjektive syn af objektivitet, der kommer til at skinne igennem. Dette er nødvendigvis ikke en dårlig ting, men det er noget mennesker skal være opmærksomme på og forstå, hvilket netop er det Æstetisk programmering gør; det sætter spørgsmålsteng ved status quo og udfordrer det, vi ikke tænker over, men i højere grad skal begynde at tænke over.
