# minix6 af Mads Leth Hansen

[RunMe](https://madseladen.gitlab.io/aestetiskprogrammering/minx6/index.html)

Billede af når man spiller spillet:
![](minix6.png)

Billede af spillet, når man har tabt:
![](Slutminix6.png)

## Spillet og hvordan det fungerer.

[Bedre overblik over koden her](https://gitlab.com/Madseladen/aestetiskprogrammering/-/blob/main/minix6/index.html)


Jeg har lavet et spil, hvor det handler om at samle mønter med en sparegris. Brugeren har 3 liv, som bliver vist oppe i hjørne hjørne og deres score bliver vist oppe i venstre hjørne.

Brugeren kan bruge piletasterne til at bevæge sparegrisen både frem og tilbage af x-aksen, men også op og ned af y-aksen. Denne funktion brugte jeg ´if(keyIsDown(){´ til at få den til at bevæge sig i de vilkårlige retninger. Og dernæst bare 3 andre if statements, der gjorde, jeg kunne bruge alle 4 piletaster i forskellige retninger. Jeg indsatte også nogen grænseværdier, der gør, at brugeren ikke kan bevæge sparegrisen ud over spillets grænser igen vha. nogen ´if-statements´, som sagde at hvis den går langt nok ud forbliver den på kanvasset.

Mønter bevæger sig modsat sparegrisen ikke hen af x-aksen, men bevæger sig udelukkende ned af y-aksen. Når de støder på sparegrisen forsviner den pågældene mønt og spawner en ny mønt lige uden for canvasset. Det er følgende kode, der gør det:

´´´
 if (distance < 50-player.size/2) {
            seddel.splice(i, 1);
            point++
            seddel.push(new Seddel());
        }
´´´

Har ses det, at hvis dsitancen imellem spilleren og variablen seddel er mindre end 50-halvdelen af spillerstørelsen, så opløses det, brugeren får et point og der bliver ´push´'et en ny seddel.

Jeg har også lavet en funktion der tjekker for, hvor mange liv brugeren har, og når brugeren mister alle sine liv, så stopper spillet, og der står GAME OVER. Dette gjorde jeg for også at give spillet en form for ende eller straf for ikke af fange mønterne.

Både mønterne samt sparegrisen er lavet i hver sin .js film, som gør det meget overskueligt bare at hive dem ind i spillet. Jeg har i takt med ugens tema brugt funktionen ´class´ til at konstruere sparegrisen samt mønterne. Jeg har desuden valgt at deres udseende skal være gifs, hvilket jeg brugte fra nettet. Dette gjorde jeg for at gøre det mere spændende at se på og måske mere engagerende for brugeren. 

## Tanker omkring koden og spillet

Det er først i den her uge, hvor det har givet mening, hvorfor vi egentlig har forskellige filer til forskellige ting. Jeg havde godt forstået, hvorfor vi havde en .html-fil og en .js-fil, men det at bruge flere .js filer er en super god måde at overskueliggøre alle ens produkter og forskellige elemeneter i en kode. Som Fuller og Goffey siger i deres tekst:

"Discussing the development of Smalltalk, Kay says: ‘object-oriented design is a successful
attempt to qualitatively improve the efficiency of modelling the ever more complex
dynamic systems and user relationships made possible by the silicon explosion’" (Fuller et al s.5, 2017)

Det er altså også en måde at effektivisere programmerne, der hele tiden bliver større ved at dele det ud udover forskellige elementer og definere her deres funktioner og deres abstraktioner. Dette var også tilfældet for mig, da jeg følte det langt mere overskueligt at finde, hvor mine fejl var samt, hvad jeg lavede forkert ved at det var delt ud på over de forskellgie elementer. 

Det giver et godt overblik og jeg har bestemt mod på at prøve mine kræfter af med flere classes i fremtiden.
