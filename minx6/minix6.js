let seddel = [];
let player;
let point = 0;
let liv = 3;
let antal = 5;
let bank;
let coin;

function preload(){
    bank = loadImage("gif.gif");
    coin=loadImage("coin.gif");
  }

function setup() {
    createCanvas(500, 500);
    background(0);
    for (let i = 0; i < antal; i++) {
        seddel.push(new Seddel());
    }

    player = new Player();
 
  

}

function draw() {
    background(0,20,20);
    spawnSeddel();
    player.show();
    checkCollision();
    checkBunden();
    textAlign(CENTER,CENTER);
    fill(255);
    textSize(20);

    text("Penge:  "+point, 50, 50)
    text("Liv: "+liv,470,50);

    if(keyIsDown(UP_ARROW)){
        player.moveUp();
    } else if(keyIsDown(DOWN_ARROW)){
        player.moveDown();
    }
    if(keyIsDown(LEFT_ARROW)){
        player.moveLeft();
    }else if(keyIsDown(RIGHT_ARROW)){
        player.moveRight();
    }
}

function spawnSeddel() {
    for (let i = 0; i < seddel.length; i++) {
        seddel[i].move();
        seddel[i].show();
    }
}



function checkCollision() {
    let distance;
    for (let i = 0; i < seddel.length; i++) {
        distance = dist(player.posX + player.size/10, player.posY + player.size/10, seddel[i].posX, seddel[i].posY);

        if (distance < 50-player.size/2) {
            seddel.splice(i, 1);
            point++
            seddel.push(new Seddel());
        }
      
    }
    
}

function checkBunden(){
    for(let i=0; i<seddel.length;i++){
        if(seddel[i].posY>height){
            liv--;
        seddel.splice(i,1);
        console.log("Hej");
        if(liv<=0){
            noLoop();
            push();
            fill(255);
            textAlign(CENTER,CENTER);
            textSize(30);
            text("GAME OVER",width/2,height/2);
            textSize(14);
            text("Tryk 'command'+'r' for at prøve igen",width/2,300)
            pop();
        }
        }
        
    }
    
    }


    //Lav så de forvandler sig, hvis man får x antal score.
    // Måske få den til at ændre størrelse random og spawne færre.
    //Lav en startsskærm/knap. 
    //Gøre det mere æstetisk at kigge på.