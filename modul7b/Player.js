class Player{
    constructor(){
        this.posX=width/2;
        this.posY=height-50;
        this.speed=20;
        this.size=50;
    }

    show(){
        fill(0,0,255);
        rect(this.posX ,this.posY, this.size);
    }

}