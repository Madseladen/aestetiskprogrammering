//Object orienteret programming beskriver et objekt i virkeligheden i kode
//Når vi programmere med data og objekter i stedet for funktioner og logikker.
//Classes og objekter kan tænkes som blueprints. Er med til at lave struktur og overblik, skalerbar og fleksibilitet, relationer og hieraki
//Opdel i 5 steps: 1. Navngivning, 2. Egenskaber/properties, 3. Behavior/method/adfærd, 4. Objekt instancering/kreering og brug, 5. Triggers: Hvordan skal objekterne relatere sig til hinanden?

let player;


function setup(){
createCanvas(500,500);
background(240);
player=new Player();
}

function draw(){
player.show();

}