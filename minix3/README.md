# minix3 af Mads Leth Hansen

Således ser min miniX ud, når man starter den:
![](minix3.png)


[Link til at køre programmet](https://madseladen.gitlab.io/aestetiskprogrammering/minix3/index.html)


[Link til koden bag det](https://gitlab.com/Madseladen/aestetiskprogrammering/-/blob/main/minix3/miniX3.js)


## Konceptet bag det.
Det startede med, jeg sad og arbejdede med geometriske former, og tænker dernæst at det kunne være sjovt at orøve a lave et kors. Dernæst kom ideen at lave en throbber til folkekirkens hjemmeside. Jeg indsatte en fugl, som flyver fra side til side, som skal repræsentere helligånden. I midten er korset, hvorpå der står Jesus Kristus på. Jeg ville ikke give mig ud i at prøve at programmere og lave jesus, så derfor endte jeg med teksten, hvilket jeg også synes giver et interessant udtryk.
Jeg har også lavet 2 cirkler, der kører op ved siden af korset, som virker for en form for afgrænsning af korset.
Baggrunden er lilla, hvilket jeg valgte, da det er guds farve.

Jeg er ikke en bestemt religiøs person, og selve ideen stammer ikke fra en videre dybdegående tanke omkring religion, men jeg synes stadig det var spændende at prøve at lave, hvoraf jeg derfor har lavet throbberen her.

Trykkes der på musen kan man se et glimt af forskellige sætninger, der skal fortælle brugeren, at de snart kommer igennem. Dette er lavet på baggrund af personlig erfaringen omkring throbberen. Jeg kan godt føle mig magtesløs, når jeg støder ind i en loadingscreen og tildels også restløs, når jeg ikke kan gøre andet end at vente. Derfor kan man med denne throbber se, at der sker noget, når man trykker og derfor også vide, at programmet ikke er gået i stykker, men at den bare loader.

## Hvordan er det lavet?
Jeg har igennem programmet både brugt lokale og globale variabler til at lave throbberen. Jeg brugte bl.a. ´let ord´ til at definere i t array alle mine forskellige sætninger, der skal stå, når der trykkes på musen.

Til at få de forskellige forme til at bevæge sig har jeg brugt denne formular:

´´´
push();
   translate(700, height/2);
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
´´´

Her bruger jeg translate til at sige, hvorhenne på canvasset, der skal være mit nue 0 punkt. Dernæst laver jeg en variable der hedder cir, som er den cirkel som min geometriske form skal dreje rundt omkring, og hvor mange grader den skal dreje rundt. Det er her med korset alle 360 grader.

Jeg gjorde også brug af `push`og `pop`til at afgrænse mine definationer og gøre så de kun gør sig gældende hos sig selv.

For at få hele funktionen omkring musetasten til at virke har jeg gjort følgende:

```
function mousePressed(){
  background(153,0,153);
  textAlign(CENTER);
  textSize(60);
  fill(0);
   text(random(ord),windowWidth/2,windowHeight/10);
}
```
Her ses det, at programmet taget en tilfældig af de sætninge, som jeg har opsat i mit array og viser det øverst i midten på canvasset. Jeg blev også nød til at lave en baggrund, når der trykkes, da man ellers ikke ville kunne se skriften.

## Tanker omkring throbbere generelt

Før dette forløb tænkte jeg slet ikke over throbbere, jeg tænkte faktisk bare det var computeren der tænkte og derfor kom dette ikon. Jeg havde ikke tanker om, at det var blevet tilført og det havde en betydning. Jeg bliver personligt altid lidt irriteret, når jeg ser en throbber, da jeg tror, jeg er blevet for vant til at kunne trykke på emner og de så vises med det samme. Jeg tror klart, hvilket også er derfor jeg har implementeret muse funktionen, at man sagtens kunne ændre synet af throbberen til noget sjovt istedet for at det altid er en irritation, der kommer frem, når man ser en throbber.

