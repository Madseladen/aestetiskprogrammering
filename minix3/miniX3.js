let ord=["Du omstilles snart til vores hjemmeside","Du er der næsten","Lige et øjeblik","Du hjælper ikke at blive ved"];
let rectX = 0; 
let rectSpeed = 5; 

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate (60);
  
 }
 
 function draw() {
  background(0,0,0,100);
  drawElements();
}


 function drawElements() {
   let num = 400;
   let num2= -100;
   let num3= 100;
   

   push();
   translate(700, height/2);
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   fill(255,255,255,200);
   noStroke();
   rectMode(CENTER);
   rect(0,0,300,100);
   rect(0,0,100,300);
   pop();

   push();
   translate(700, height/2);
   let cir2 = 360/num2*(frameCount%num2);
   rotate(radians(cir2));
   noStroke();
   fill(255,255,0);
   stroke(20);
   ellipse(300,300,100,100);
   pop();

   push();
   translate(700, height/2);
   let cir3 = 360/num3*(frameCount%num3);
   rotate(radians(cir3));
   noStroke();
   fill(255,255,0);
   stroke(20);
   ellipse(-300,300,100,100);
   pop();

  
}
 


function mousePressed(){
  background(0);
  textAlign(CENTER);
  textSize(60);
  fill(255);
   text(random(ord),random(100,1100),random(100,1100));
}
